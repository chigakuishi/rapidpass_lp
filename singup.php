<?
$fp=fopen("camp.csv","r");
$num=0;
while($line=fgets($fp)){
	//rtrim($line);
	$line = preg_replace('/\n|\r|\r\n/', '', $line );
	//print $line;
	$dat[$num]=$line;
	$num++;
}
fclose($fp);
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>先行登録</title>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script>
			var dat=Array(1024);
			var num=Number(<?echo $num;?>);
			var camp_id=0;
			<?
				for($i=0;$i<$num;$i++){
					echo "dat[$i]=\"$dat[$i]\";  ";
				}
			?>
/*			function make_opt(value){
				return "<option value=\""+value+"\">"+value+"</option>\n";
			}*/
			function make_opt(value,inner){
				return "<option value=\""+value+"\">"+inner+"</option>\n";
			}
			function chk_mail(input){
				var match=input.match(/^(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+))*)|(?:"(?:\\[^\r\n]|[^\\"])*")))\@(?:(?:(?:(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+)(?:\.(?:[a-zA-Z0-9_!#\$\%&'*+/=?\^`{}~|\-]+))*)|(?:\[(?:\\\S|[\x21-\x5a\x5e-\x7e])*\])))$/);
				if(match==null)return false;
				else return true;
			}
			function send_get(dat){
				var url="http://web.sfc.wide.ad.jp/~rhenium/test/final.php"+dat; // リクエスト先URL
				var request = new XMLHttpRequest();
				request.open('GET', url);
				request.onreadystatechange = function () {
						if (request.readyState != 4) {
								return 0;
						} else if (request.status != 200) {
								return -1;
						} else {
								return 1;
								// 取得成功
								// var result = request.responseText;
						}
				};
				request.send(null);
			}
			function select_tdfk(){
				var tmp=new Object();
				var value=document.getElementById("tdfk").value;
				if(value==""){return;}
				var out="<option value=\"\">------</option>";
				document.getElementById("univ").style.display="block";
				document.getElementById("camp").style.display="none";
				document.getElementById("mail").style.display="none";
				var reg = new RegExp(value + ",([^,]+),[^,]*$");
				for(var i=0;i<num;i++){
					var match=dat[i].match(reg);
					if(match==null){continue;}
					if(tmp[match[1]]==null){
						tmp[match[1]]=1;
						out=out+make_opt(match[1],match[1]);
					}
				}
				document.getElementById("univ").innerHTML=out;
			}
			function select_end(flag){
				var value1=document.getElementById("tdfk").value;
				var value2=document.getElementById("univ").value;
				if(value1==""||value2==""){return;}
				if(flag==-1){
					var value3=document.getElementById("camp").value;
					if(value3==""){return;}
					flag=value3;
				}
				camp_id=flag;
				//alert(dat[Number(flag)]);
				document.getElementById("mail").style.display="block";
			}
			function all_end(){
				var mail_add=document.getElementById("mail_f").value;
				if(!chk_mail(mail_add)){
					alert("不正なメールアドレスです！！");
					return false;
				}
				//send_get("?id="+camp_id+"&mail="+encodeURIComponent(mail_add));
				//document.getElementById("write").style.display="none";
				//document.getElementById("comp").style.display="block";
				document.getElementById("hidden_id").value=camp_id;
				return true;
			}
			function select_univ(){
				var tmp=new Object();
				var value1=document.getElementById("tdfk").value;
				var value2=document.getElementById("univ").value;
				if(value1==""||value2==""){return;}
				var out="<option value=\"\">-----</option>";
				var reg = new RegExp(value1 + ","+value2+",([^,]+)$");
				var reg2 = new RegExp(value1 + ","+value2+",$");
				for(var i=0;i<num;i++){
					var match =dat[i].match(reg );
					var match2=dat[i].match(reg2);
					if(match2!=null){
						document.getElementById("camp").style.display="none";
						select_end(i);
						return;
					}
					if(match==null){continue;}
					if(tmp[match[1]]==null){
						tmp[match[1]]=1;
						out=out+make_opt(""+i,match[1]);
					}
				}
				document.getElementById("camp").innerHTML=out;
				document.getElementById("camp").style.display="block";
				document.getElementById("mail").style.display="none";
			}
		</script>
	</head>

	<body>
		<form method="POST" action="final.php" onsubmit="return all_end();">
			<div id="write">
				都道府県名<br>
				<select id="tdfk"  onchange="select_tdfk();"><!--{{{-->
					<option value="">-------</option>
					<option value="北海道">北海道</option>
					<option value="青森県">青森県</option>
					<option value="秋田県">秋田県</option>
					<option value="岩手県">岩手県</option>
					<option value="山形県">山形県</option>
					<option value="宮城県">宮城県</option>
					<option value="新潟県">新潟県</option>
					<option value="福島県">福島県</option>
					<option value="栃木県">栃木県</option>
					<option value="茨城県">茨城県</option>
					<option value="群馬県">群馬県</option>
					<option value="埼玉県">埼玉県</option>
					<option value="千葉県">千葉県</option>
					<option value="東京都">東京都</option>
					<option value="神奈川県">神奈川県</option>
					<option value="富山県">富山県</option>
					<option value="長野県">長野県</option>
					<option value="山梨県">山梨県</option>
					<option value="静岡県">静岡県</option>
					<option value="岐阜県">岐阜県</option>
					<option value="愛知県">愛知県</option>
					<option value="石川県">石川県</option>
					<option value="福井県">福井県</option>
					<option value="滋賀県">滋賀県</option>
					<option value="三重県">三重県</option>
					<option value="京都府">京都府</option>
					<option value="奈良県">奈良県</option>
					<option value="和歌山県">和歌山県</option>
					<option value="大阪府">大阪府</option>
					<option value="兵庫県">兵庫県</option>
					<option value="兵庫県">兵庫県</option>
					<option value="鳥取県">鳥取県</option>
					<option value="岡山県">岡山県</option>
					<option value="島根県">島根県</option>
					<option value="広島県">広島県</option>
					<option value="山口県">山口県</option>
					<option value="香川県">香川県</option>
					<option value="徳島県">徳島県</option>
					<option value="愛媛県">愛媛県</option>
					<option value="高知県">高知県</option>
					<option value="福岡県">福岡県</option>
					<option value="大分県">大分県</option>
					<option value="宮崎県">宮崎県</option>
					<option value="鹿児島県">鹿児島県</option>
					<option value="熊本県">熊本県</option>
					<option value="佐賀県">佐賀県</option>
					<option value="長崎県">長崎県</option>
					<option value="沖縄県">沖縄県</option>
				</select><!--}}}-->
				<br>
				大学名
				<select id="univ" onchange="select_univ();">
				</select>
				<br>
				キャンパス名
				<select id="camp"  onchange="select_end(-1);">
				</select>
				<br>
				<br>
				メールアドレス
				<div id="mail">
					<input type="email" id="mail_f" name="mail">
					<!--
						<input type="submit" value="登録完了" onclick="all_end();">
					-->
					
					<div class="g-recaptcha" data-sitekey="6Ld2owoTAAAAAPCS6TDZSg0aDE4c0YywEvZv2XT7"></div>
					
					<!--
						<php
							require_once('recaptchalib.php');
							$publickey = "6Ld2owoTAAAAAPCS6TDZSg0aDE4c0YywEvZv2XT7"; // you got this from the signup page
							echo recaptcha_get_html($publickey);
						?>
					-->
					<br>
					<input type="submit" value="登録完了">
				</div>
			</div>
			<input type="hidden" id="hidden_id" name="id" value="">
		</form>
		<div id="comp">
			登録完了しました
		</div>
	</body>
	<script>
		document.getElementById("univ").style.display="none";
		document.getElementById("camp").style.display="none";
		document.getElementById("mail").style.display="none";
		document.getElementById("comp").style.display="none";
	</script>
</html>
